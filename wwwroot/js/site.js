﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

window.onload = main;

async function deleteHandler(id) {
    await fetch(`home/delete/${id}`, {
        method: 'DELETE',
        mode: 'cors',
        headers: {'Content-Type': 'application/json' }
    });

    window.location.reload();
}

function main() {
    const deleteActionElements = document.querySelectorAll(".delete-action");

    for(const element of deleteActionElements) {
        element.addEventListener("click", () => {
            deleteHandler(element.getAttribute("id-value"));
        });
    }
}