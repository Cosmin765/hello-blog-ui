
using System.ComponentModel.DataAnnotations;

namespace hello_blog_ui.Models
{
    public class BlogPost
    {
        public int Id { get; set; }
        [Required, MaxLength(80)]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required, MaxLength(50)]
        public string Label { get; set; }
    }
}
