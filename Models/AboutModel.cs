namespace hello_blog_ui.Models
{
    public class AboutModel
    {
        public string Content { get; set; }
        public string GithubLink { get; set; }
        public string GameLink { get; set; }
    }
}
